# Ruby as a Mac Framework

Instead of strange install locations or the overuse of /usr/local, why not use Apple's well thought out, [stow](https://www.gnu.org/software/stow/manual/stow.html)-like layout, [Frameworks](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPFrameworks/Concepts/FrameworkAnatomy.html)?

I don't know why not, so here are some scripts to help install Ruby that way.

Run the Ruby script last.