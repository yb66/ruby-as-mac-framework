#!/bin/bash

# A script to install GNU database manager as a per-user framework on a Mac.
# See https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPFrameworks/Concepts/FrameworkAnatomy.html for more on frameworks.

GDBM_VERSION=${1:-1.9.1}
FRAMEWORK_ROOT=$HOME/Library/Frameworks/GDBM.framework
FRAMEWORK_VERSIONS=$FRAMEWORK_ROOT/Versions

if [ -d $FRAMEWORK_VERSIONS/$GDBM_VERSION ]; then
  echo "This version of the framework appears to already have been installed."
  exit 0
else
  mkdir -p $FRAMEWORK_VERSIONS

  # Useful for restarting, like when the download has already been done
  # but the script failed later on.
  mytmpdir=${2:-`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`}
  pushd $mytmpdir

  if ! [ -e gdbm-$GDBM_VERSION.tar.gz ]; then
    curl --remote-name https://mirror.leifrogers.com/gnu/gdbm/gdbm-$GDBM_VERSION.tar.gz
  fi

  if ! [ -d gdbm-$GDBM_VERSION ]; then
    tar -xzvf gdbm-$GDBM_VERSION.tar.gz
  fi

  cd gdbm-$GDBM_VERSION

  ./configure --prefix=$FRAMEWORK_VERSIONS/$GDBM_VERSION --enable-libgdbm-compat
  make
  make install

  cd $FRAMEWORK_VERSIONS

  if [ -L Current ]; then
    rm Current
  fi
  ln -s $GDBM_VERSION Current

  cd $GDBM_VERSION

  if [ -d bin ]; then
    if ! [ -L Programs ]; then
      ln -s bin Programs
    fi
  fi

  if [ -d includes ]; then
    if ! [ -L Headers ]; then
      ln -s includes Headers
    fi
  fi

  if [ -d lib ]; then
    if ! [ -L Libraries ]; then
      ln -s lib Libraries
    fi
  fi

  cd $FRAMEWORK_ROOT


  if [ -L Programs ]; then
    rm Programs
  fi
  if [ -d "Versions/Current/bin" ]; then
    ln -s Versions/Current/bin Programs
  fi


  if [ -L Headers ]; then
    rm Headers
  fi
  if [ -d "Versions/includes" ]; then
    ln -s Versions/Current/includes Headers
  fi

  if [ -L Libraries ]; then
    rm Libraries
  fi
  if [ -d "Versions/Current/lib" ]; then
    ln -s Versions/Current/lib Libraries
  fi


  popd

  echo "GDBM $GDBM_VERSION installation has finished "
  exit 0
fi