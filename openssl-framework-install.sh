#!/bin/bash

# A script to install OpenSSL as a per-user framework on a Mac.
# With help from https://mac-dev-env.patrickbougie.com/openssl/
# See https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPFrameworks/Concepts/FrameworkAnatomy.html for more on frameworks.

OPENSSL_VERSION=${1:-1.1.1a}
FRAMEWORK_ROOT=$HOME/Library/Frameworks/OpenSSL.framework
FRAMEWORK_VERSIONS=$FRAMEWORK_ROOT/Versions

if [ -d $FRAMEWORK_VERSIONS/$OPENSSL_VERSION ]; then
  echo "This version of the framework appears to already have been installed."
  exit 0
else
  mkdir -p $FRAMEWORK_VERSIONS

  # Useful for restarting, like when the download has already been done
  # but the script failed later on.
  mytmpdir=${2:-`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`}
  pushd $mytmpdir

  if ! [ -e openssl-$OPENSSL_VERSION.tar.gz ]; then
    curl --remote-name https://www.openssl.org/source/openssl-$OPENSSL_VERSION.tar.gz
  fi

  if ! [ -d openssl-$OPENSSL_VERSION ]; then
    tar -xzvf openssl-$OPENSSL_VERSION.tar.gz
  fi

  cd openssl-$OPENSSL_VERSION

  ./config --prefix=$FRAMEWORK_VERSIONS/$OPENSSL_VERSION
  make
  make install

  cd $FRAMEWORK_VERSIONS

  if [ -L Current ]; then
    rm Current
  fi
  ln -s $OPENSSL_VERSION Current

  cd $OPENSSL_VERSION

  if [ -d bin ]; then
    if ! [ -L Programs ]; then
      ln -s bin Programs
    fi
  fi

  if [ -d includes ]; then
    if ! [ -L Headers ]; then
      ln -s includes Headers
    fi
  fi

  if [ -d lib ]; then
    if ! [ -L Libraries ]; then
      ln -s lib Libraries
    fi
  fi

  cd $FRAMEWORK_ROOT


  if [ -L Programs ]; then
    rm Programs
  fi
  if [ -d "Versions/Current/bin" ]; then
    ln -s Versions/Current/bin Programs
  fi


  if [ -L Headers ]; then
    rm Headers
  fi
  if [ -d "Versions/includes" ]; then
    ln -s Versions/Current/includes Headers
  fi

  if [ -L Libraries ]; then
    rm Libraries
  fi
  if [ -d "Versions/Current/lib" ]; then
    ln -s Versions/Current/lib Libraries
  fi


  mkdir openssl

  security find-certificate -a -p /Library/Keychains/System.keychain > $FRAMEWORK_ROOT/openssl/ssl/cert.pem
  security find-certificate -a -p /System/Library/Keychains/SystemRootCertificates.keychain >> $FRAMEWORK_ROOT/openssl/ssl/cert.pem

  popd

  echo "Run openssl version -a to verify the installation"
  exit 0
fi