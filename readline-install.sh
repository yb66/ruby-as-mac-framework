#!/bin/bash

# A script to install Readline as a per-user framework on a Mac.
# See https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPFrameworks/Concepts/FrameworkAnatomy.html for more on frameworks.

READLINE_VERSION=${1:-8.0}
FRAMEWORK_ROOT=$HOME/Library/Frameworks/Readline.framework
FRAMEWORK_VERSIONS=$FRAMEWORK_ROOT/Versions


if [ -d $FRAMEWORK_VERSIONS/$READLINE_VERSION ]; then
  echo "This version of the framework appears to already have been installed."
  exit 0
else
  mkdir -p $FRAMEWORK_VERSIONS

  mytmpdir=${2:-`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`}
  pushd $mytmpdir

  if ! [ -e readline-$READLINE_VERSION.tar.gz ]; then
    curl --remote-name https://ftp.gnu.org/gnu/readline/readline-$READLINE_VERSION.tar.gz
  fi

  if ! [ -d readline-$READLINE_VERSION ]; then
    tar -xzvf readline-$READLINE_VERSION.tar.gz
  fi

  cd readline-$READLINE_VERSION

  ./configure --prefix=$FRAMEWORK_VERSIONS/$READLINE_VERSION --enable-multibyte
  make
  make install

  cd $FRAMEWORK_VERSIONS

  if [ -L Current ]; then
    rm Current
  fi
  ln -s $READLINE_VERSION Current

  cd $READLINE_VERSION

  if [ -d bin ]; then
    if ! [ -L Programs ]; then
      ln -s bin Programs
    fi
  fi

  if [ -d includes ]; then
    if ! [ -L Headers ]; then
      ln -s includes Headers
    fi
  fi

  if [ -d lib ]; then
    if ! [ -L Libraries ]; then
      ln -s lib Libraries
    fi
  fi

  cd $FRAMEWORK_ROOT


  if [ -L Programs ]; then
    rm Programs
  fi
  if [ -d "Versions/Current/bin" ]; then
    ln -s Versions/Current/bin Programs
  fi


  if [ -L Headers ]; then
    rm Headers
  fi
  if [ -d "Versions/includes" ]; then
    ln -s Versions/Current/includes Headers
  fi

  if [ -L Libraries ]; then
    rm Libraries
  fi
  if [ -d "Versions/Current/lib" ]; then
    ln -s Versions/Current/lib Libraries
  fi


  popd

  echo "Installation of Readline $READLINE_VERSION finished."
  exit 0
fi