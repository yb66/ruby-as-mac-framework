#!/bin/bash

RUBY_VERSION=${1:-2.6.0}
RUBY_INSTALL_VERSION=${2:-0.7.0}
FRAMEWORK_ROOT=$HOME/Library/Frameworks/Ruby.framework
FRAMEWORK_VERSIONS=$FRAMEWORK_ROOT/Versions
THE_RUBY=$FRAMEWORK_VERSIONS/$RUBY_VERSION


if [ -d $FRAMEWORK_VERSIONS/$RUBY_VERSION ]; then
  echo "This version of the framework appears to already have been installed."
  exit 0
else
  mkdir -p $FRAMEWORK_VERSIONS

  # clang is more than likely required for this to work
  # see https://github.com/postmodern/ruby-install/issues/181
  if ![ -x "$(command -v clang)" ]; then
    echo "Please ensure clang is present on the system, probably by installing Xcode."
    exit 1
  fi

  # Make sure ruby-install is installed.
  if ![ -x "$(command -v ruby-install)" ]; then
    # Useful for restarting, like when the download has already been done
    # but the script failed later on.
    mytmpdir=${3:-`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`}
    pushd $mytmpdir
    if ! [ -e ruby-install-$RUBY_INSTALL_VERSION.tar.gz ]; then
      curl --remote-name ruby-install-$RUBY_INSTALL_VERSION.tar.gz https://github.com/postmodern/ruby-install/archive/v$RUBY_INSTALL_VERSION.tar.gz
    fi

    if ! [ -d ruby-install-$RUBY_INSTALL_VERSION ]; then
      tar -xzvf ruby-install-0.7.0.tar.gz
    fi

    cd ruby-install-$RUBY_INSTALL_VERSION/
    make install
  fi

  # Testing again ensures the installation was done
  # TODO Should put in checks for the frameworks too.
  if [ -x "$(command -v ruby-install)" ]; then
    CC=clang ruby-install ruby \
                          --install-dir $FRAMEWORK_VERSIONS/$RUBY_VERSION -- \
                          --with-openssl-dir=$OPENSSL_DIR \
                          --with-gdbm-dir=$GDBM_DIR \
                          --with-readline-dir=$READLINE_DIR
  fi

  cd $FRAMEWORK_VERSIONS
  if [ -L Current ]
    rm Current
  fi
  ln -s $RUBY_VERSION Current
fi

echo "Ruby $RUBY_VERSION installation has finished."
exit 0